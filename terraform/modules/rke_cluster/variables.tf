variable "node_address" {
  type        = string
  description = "IP address or FQDN of the node to install Kubernetes on."
}

variable "ssh_user" {
  type        = string
  description = "SSH user on the node that can install Kubernetes (must have passwordless sudo access)."
}

variable "k8s_role" {
  default     = ["controlplane", "worker", "etcd"]
  type        = list
  description = "Kubernetes roles that the node will perform."
}