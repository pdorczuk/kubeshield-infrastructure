// Create the Kubernetes cluster
resource "rke_cluster" "cluster" {
  nodes {
    address = var.node_address
    user    = var.ssh_user
    role    = var.k8s_role
    ssh_key = file("~/.ssh/id_phil")
  }
  upgrade_strategy {
    drain = true
    max_unavailable_worker = "20%"
  }
}

// Save the kubeconfig file to the current directory
resource "local_file" "kube_cluster_yaml" {
  filename = "${path.root}/kube_config_cluster.yml"
  sensitive_content  = "${rke_cluster.cluster.kube_config_yaml}"
}