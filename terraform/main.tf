provider "rke" {
  log_file = "rke_debug.log"
}

// Create the Kubernetes cluster
module "rke_cluster" {
  source = "./modules/rke_cluster"

  node_address = "192.168.1.211"
  ssh_user = "phil"

}